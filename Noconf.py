from Punkt import Punkt
from Fibo import Fibo

if __name__ == '__main__':
    print('Pierwszy feature')
    print('Drugi feature')
    p1 = Punkt(2, 3, 4)
    print("Współżędne punktu: " + str(p1.x) + ' ' + str(p1.y) + ' ' + str(p1.z) + ' ')
    print("Odleglosc pod ponktu 0 0 0: ")
    p1.distance(0, 0, 0)
    print("13 wyraz Fibo: " + str(Fibo.Fibonacci(10)))
