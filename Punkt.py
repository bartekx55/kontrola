
class Punkt:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def distance(self, x2, y2, z2):
        dist = ((x2 - self.x)**2 + (y2 - self.y)**2 + (z2 - self.z)**2) ** (1/2)
        print("Distance from " + str(x2) + ' ' + str(y2) + ' ' + str(z2) + ' = ' + str(dist))
