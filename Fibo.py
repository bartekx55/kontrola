class Fibo:
    
    @staticmethod
    def Fibonacci(n):

        if n < 0:
            print("Incorrect input")

        elif n == 0:
            return 0

        elif n == 1 or n == 2:
            return 1

        else:
            return Fibo.Fibonacci(n - 1) + Fibo.Fibonacci(n - 2)


